#include<stdio.h>
#include<math.h>
struct point
{
    int x;
    int y;
};
typedef struct point Point;
Point input()
{
    Point p;
    printf("Enter the X coordinate \n");
    scanf("%d",&p.x);
    printf("Enter the Y coordinate \n");
    scanf("%d",&p.y);
    return p;
}

float distance(Point p1, Point p2)
{
    float dist=sqrt((pow(p1.x-p2.x,2))+(pow(p1.y-p2.y,2)));
    return dist;
}

void display(Point p1, Point p2,float a)
{
    printf("The distance of points (%d,%d) and (%d,%d) is %f\n",p1.x,p1.y,p2.x,p2.y,a);
}

int main()
{
    Point p1=input();
    Point p2=input();
    float dist=distance(p1,p2);
    display(p1,p2,dist);
    return 0;
}