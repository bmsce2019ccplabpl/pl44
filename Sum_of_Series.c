#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    return n;
}
float term(int a)
{
    float den=1;
    for(int i=1;i<=a;i++)
    {
        den=den*i;
    }
    float value=(float)a/den;
    return value;
}
void display(float a)
{
    printf("= %f\n",a);
}


int main()
{
    int n=input();
    
    float sol=1;
    printf("1/1! ");
    for(int i=2;i<=n;i++)
    {
        float x=term(i);
        if((i%2)==0)
        {
            printf("+ %d/%d! ",i,i);
            sol=sol+x;
        }
        else
        {
            printf("- %d/%d! ",i,i);
            sol=sol-x;
        }
    }
    
    display(sol);
    return 0;
}
        