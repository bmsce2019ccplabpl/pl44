#include<stdio.h>
struct Employee
{
    char name[100];
    char eno[20];
    char dob[10];
    char salary[10];
};
typedef struct Employee emp;

emp input()
{
    emp a;
    printf("Enter the Name\n");
    gets(a.name);
    printf("Enter the Employee no \n");
    gets(a.eno);
    printf("Enter the Date of Birth \n");
    gets(a.dob);
    printf("Enter the Salary \n");
    gets(a.salary);
    return a;
}
void display( emp a)
{
    printf("\nNAME:");
    puts(a.name);
    printf("\nEMPLOYEE NUMBER:");
    puts(a.eno);
    printf("\nDOB:");
    puts(a.dob);
    printf("\nSALARY:");
    puts(a.salary);
}
int main()
{
    emp e=input();
    display(e);
    return 0;
}