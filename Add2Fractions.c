#include<stdio.h>
struct fraction
{
    int nom;
    int den;
};
typedef struct fraction fract;

fract input()
{
    fract a;
    printf("  Enter the nominator\n");
    scanf("%d",&a.nom);
    printf("\tEnter the Denominator\n");
    scanf("%d",&a.den);
    return a;
}

fract compute(fract a, fract b)
{
    fract sum;
    int lcm;
    if(a.den!=b.den)
    {
        lcm=(a.den>b.den)? a.den:b.den;
        while(1)
        {
            if(lcm%a.den==0 && lcm%b.den==0)
            break;
            ++lcm;
        }
    
        int c=lcm/a.den;
        int d=lcm/b.den;
        sum.nom=(a.nom*c)+(b.nom*d);
        sum.den=lcm;
    return sum;
    }
    
    else
    {
        sum.nom=a.nom+b.nom;
        sum.den=a.den;
    }
    return sum;
}

void display(fract a, fract b, fract c)
{
    printf("The sum of %d/%d and %d/%d is %d/%d\n",a.nom,a.den,b.nom,b.den,c.nom,c.den);
}

int main()
{   
    printf("1st NO");
    fract a=input();
    printf("2nd NO");
    fract b=input();
    fract c=compute(a,b);
    display(a,b,c);
    return 0;
}