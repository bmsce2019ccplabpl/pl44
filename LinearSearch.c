#include<stdio.h>
int main()
{
    int n;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter %d numbers\n",n);
    
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    
    int key;
    printf("Enter the number to be searched\n");
    scanf("%d",&key);
    
    int m=0, pos=0;
    
    for(int i=0;i<n;i++)
    {
        if(a[i]==key)
        {
            m=1;
            pos=i;
            break;
        }
    }
    
    if(m==1)
        printf("The number %d was found at position %d\n",key,pos);
    else
        printf("Number not found\n");
        
    return 0;
}